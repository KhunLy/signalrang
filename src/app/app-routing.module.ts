import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './_components/login/login.component';
import { IsNotLoggedGuard } from './_guards/is-not-logged.guard';
import { ChatComponent } from './_components/chat/chat.component';
import { IsLoggedGuard } from './_guards/is-logged.guard';


const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [IsNotLoggedGuard] },
  { path: '', component: ChatComponent, canActivate: [IsLoggedGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

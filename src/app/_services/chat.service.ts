import { Injectable } from '@angular/core';
import * as sr from '@aspnet/signalr';
import { SessionService } from './session.service';
import { Subject } from 'rxjs';
import { MessageModel } from '../_models/message.model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  

  public newMessage$: Subject<MessageModel>;
  public Users$: Subject<string[]>;

  private hubConnection: sr.HubConnection;

  constructor(
    private session: SessionService
  ) {
    this.Users$ = new Subject<string[]>();
    this.newMessage$ = new Subject<MessageModel>();
    this.hubConnection = new sr.HubConnectionBuilder()
      .withUrl("http://localhost:2020/chathub").build();
    this.hubConnection.start().then(conn => {
      this.hubConnection.invoke("Connect", session.username);

      this.hubConnection.on("REFRESH_USER", (users) => {
        this.Users$.next(users);
      });

      this.hubConnection.on("NEW_MESSAGE", (m) => {
        this.newMessage$.next(m);
      });

    }).catch(e => console.log(e));
  }

  send(m: MessageModel) {
    this.hubConnection.invoke("Send", m);
  }
}

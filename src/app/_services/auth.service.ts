import { Injectable } from '@angular/core';
import { LoginModel } from '../_models/login.model';
import { SessionService } from './session.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private session: SessionService,
    private router: Router
  ) { }

  login(value: LoginModel) {
    this.session.username = value.Username;
    this.router.navigateByUrl('/');
  }
}

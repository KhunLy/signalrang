import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  token: string;
  userId: number;
  username: string;


  constructor() { }
}

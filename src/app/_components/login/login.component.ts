import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  fg: FormGroup;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.fg = new FormGroup({
      Username: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      Password: new FormControl(null, Validators.compose([
        Validators.required
      ]))
    });
  }

  onSubmit() {
    this.authService.login(this.fg.value);
  }

}

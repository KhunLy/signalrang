import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/_services/chat.service';
import { MessageModel } from 'src/app/_models/message.model';
import { SessionService } from 'src/app/_services/session.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {


  users: any[] = [];
  currentUser: any;
  content: string;

  constructor(
    private chatService: ChatService,
    private session: SessionService
    ) { }

  ngOnInit() {
    this.chatService.Users$.subscribe(data => {
      this.users = data;
    })
  }

  change(username: any) {
    this.currentUser = username;
  }

  send() {
    let m = <MessageModel>{ 
      author: this.session.username,
      recipient: this.currentUser,
      date: new Date(),
      content: this.content
    };

    this.chatService.send(m);

    this.content = null;
  }

}

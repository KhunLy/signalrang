import { Component, OnInit, Input } from '@angular/core';
import { MessageModel } from 'src/app/_models/message.model';
import { SessionService } from 'src/app/_services/session.service';
import { ChatService } from 'src/app/_services/chat.service';

@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss']
})
export class DiscussionComponent implements OnInit {

  public currentKey: string;

  @Input() set username(v: string){
    this.currentKey = v;
    if(this.messages[v] == null){
      this.messages[v] = [];
    }
  }

  messages: {[id: string] : MessageModel[]} = {};

  constructor(
    public session: SessionService,
    private chatService: ChatService
  ) { }

  ngOnInit() {
    this.chatService.newMessage$.subscribe(m => {
      let key = this.session.username == m.author ? m.recipient : m.author;
      if(this.messages[key] == null) this.messages[key] = [];
      this.messages[key].push(m);
    });
  }

}

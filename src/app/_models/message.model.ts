export interface MessageModel {
    id: number;
    authorId: number;
    author: any;
    recipientId: number;
    recipient: any;
    date: Date;
    content: string;
}